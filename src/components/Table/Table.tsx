import React, { useState, useEffect } from "react"
import Cell from "../Cell/Cell"
import "./Table.css"
 
const Table = () => {
  const emptyCells: string[] = Array(9).fill("")
  const [cells, setCells] = useState(emptyCells)
 
  const cellClicked = (idx: number) => {
    const squares: string[] = [...cells]
    const freeSquares: string[] = squares.filter((item) => item === "")
    const squaresLeft: number = freeSquares.length
    const squaresPicked: string = squares[idx]
    
    if (squaresPicked) {
      alert("This was already picked")
      return
    }
 
    const currentTurn = squaresLeft % 2 === 0 ? "X" : "O"
    const newCells = squares.map((s, i) => {  
      if (i === idx) { 
        return currentTurn
      }
      return s
    })
 
    setCells(newCells)
  }
 
  const calculateWinner = () => {
    const winCombo: number[][] = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ]
    for (let i = 0; i < winCombo.length; i++) {
      const [a, b, c] = winCombo[i]
      if (cells[a] && cells[a] === cells[b] && cells[a] === cells[c]) {
        return cells[a]
      }
    }
    return null
  }
  const resetGame = (): void => {
    setCells(emptyCells)
  }
  useEffect((): void => {
    const winner = calculateWinner()
 
    if (winner) {
      alert(`player ${winner} has won the game`)
      resetGame()
    }
  }, [cells])
 
  return (
    <>
      <div className="table">
        <div className="row">
          <Cell cell={cells[0]} clicked={() => cellClicked(0)} />
          <Cell cell={cells[1]} clicked={() => cellClicked(1)} />
          <Cell cell={cells[2]} clicked={() => cellClicked(2)} />
        </div>
        <div className="row">
          <Cell cell={cells[3]} clicked={() => cellClicked(3)} />
          <Cell cell={cells[4]} clicked={() => cellClicked(4)} />
          <Cell cell={cells[5]} clicked={() => cellClicked(5)} />
        </div>
        <div className="row">
          <Cell cell={cells[6]} clicked={() => cellClicked(6)} />
          <Cell cell={cells[7]} clicked={() => cellClicked(7)} />
          <Cell cell={cells[8]} clicked={() => cellClicked(8)} />
        </div>
      </div>
      <button onClick={() => resetGame()}>clear game</button>
    </>
  )
}
 
export default Table