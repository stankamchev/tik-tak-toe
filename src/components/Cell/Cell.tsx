import React from 'react'
import "./Cell.css"
type Props = {
    cell : string
    clicked() : void 
}
  
const Cell = ({cell, clicked} : Props) => {
    return (
        <div className="cell-item" onClick={clicked}>
            <span className="cell">{cell}</span>
        </div>
    )
}

export default Cell
