import React from 'react';
import Table from "./components/Table/Table"
function App() {
  return (
    <div style={{display: "Flex", flexDirection: "column", alignItems: "center"}} className="App">
      <Table/>
    </div>
  );
}

export default App;
